FROM node:lts-alpine

WORKDIR /usr/src/app

ARG NODE_ENV
ENV NODE_ENV $NODE_ENV

COPY . /usr/src/app
RUN yarn install

ENV PORT 5000
EXPOSE $PORT
CMD [ "yarn", "start" ]
